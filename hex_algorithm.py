def hex(number):
    hexDec = ["A","B","C","D","E","F"]
    return hexDec[number-10]

def convert(decimal):
    hexNum = int(decimal / 16)
    hexRem = int(decimal % 16)

    if hexNum >= 10:
        hexNum = hex(hexNum)
    if hexRem >= 10:
        hexRem = hex(hexRem)

    print(hexNum, hexRem, sep='')

def main():
    intNum = False
    while intNum == False:
        try:
            decimal = int(input("\nPlease enter a number between 0 and 255: "))
            if decimal > 0 and decimal < 256:
                intNum = True
            else:
                print("ERROR: The number needs to be between 0 and 256")
        except:
            print("ERROR: You need to enter an integer")

    convert(decimal)

main()
input("Press enter to continue...")
