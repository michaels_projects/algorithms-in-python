# import the random library
import random

# make a list of items
def makeList():
    # ask how many items you want
    items = int(input("How many list items would you like?: "))
    high = 10
  	# create a new list
    randomList = []
    low = 0
    
    # add items into the list
    for x in range(1,items+1):
        randomList.append(random.randint(low,high))
        low = low + 12
        high = high + 12
        
    # return the list
    print("Sorted: " + str(randomList))
    return(randomList)

def binarySearch():
    binList = makeList()
    target = int(input("What number are you looking for?: "))
    found = False
    steps = 1
    middle = int(len(binList)/2)
    
    while found != True and middle > 1:
        steps = steps + 1    
        middle = int(len(binList)/2)
        
        if binList[middle] == target:
            found = True
            break
        elif binList[middle] > target:
            binList = binList[:middle]
        elif binList[middle] < target:
            binList = binList[middle:]

    if found != True:
        print(str(steps) + " steps taken: NOT PRESENT in array")
    else:
        print(str(steps) + " steps taken: PRESENT in array")

    return steps

binarySearch()
