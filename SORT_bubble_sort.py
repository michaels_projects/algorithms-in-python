# import the random library
import random

# define a function
# items = total items, high = highest potential number
def makeUnsortedList(high):
    # ask how many items you want
    items = int(input("\nHow many list items would you like?: "))
    # create a new list
    randomList = []
    low = 0
    
    # add items into the array
    for x in range(1,items+1):
        randomList.append(random.randint(low,high))
        
    # return the array
    print("Unsorted: " + str(randomList))
    return(randomList)

def bubble(list_a):
    indexing_length = len(list_a)-1
    sorted = False
    
    while not sorted:
        sorted = True
        for i in range(0, indexing_length):
            if list_a[i] > list_a[i+1]:
                sorted = False
                list_a[i], list_a[i+1] = list_a[i+1], list_a[i]
    return list_a

print("  Sorted: " + str(bubble(makeUnsortedList(100))) + "\nBubble Sort Complete")
input("Press enter to continue...")
