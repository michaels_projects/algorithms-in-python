# import the random library
import random

# make an List of items
def makeList():
    # ask how many items you want
    items = int(input("\nHow many list items would you like?: "))
    high = 10
    # create a new list
    randomList = []
    low = 0
    
    # add items into the list
    for x in range(1,items+1):
        randomList.append(random.randint(low,high))
        low = low + 12
        high = high + 12
        
    # return the list
    print(randomList)
    return(randomList)

def linearSearch():
    linList = makeList()
    target = int(input("\nWhat number should I search for?: "))
    found = False
    
    for x in range(0,len(linList)-1):
        if linList[x] == target:
            print("\n>>> Found in position: [ " + str(x) + " ] <<<")
            found = True
    if found != True:
        print("\n[ Not found ]")
    print("End of list reached\n")
    
linearSearch()
input("Press enter to continue...")

