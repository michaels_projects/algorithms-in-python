# import the random library
import random

# make an List of items
def makeList():
    # ask how many items you want
    items = int(input("\nHow many list items would you like?: "))
    # create a new list
    randomList = []
    
    # add items into the list
    for x in range(1,items+1):
        randomList.append(random.randint(0,100))
        
    # return the list
    print("Unsorted: " + str(randomList))
    return(randomList)

def quickSort(sequence):
    length = len(sequence)
    if length <= 1:
        return sequence
    else:
        pivot = sequence.pop()
        # print("Pivot used: " + str(pivot))

    lower = []
    higher = []
    
    for item in sequence:
        if item <= pivot:
            lower.append(item)
        elif item > pivot:
            higher.append(item)

    #print(higher)
    #print(lower)

    return quickSort(lower) + [pivot] + quickSort(higher)
    
print("  Sorted: " + str(quickSort(makeList())) + "\nQuick Sort Complete")
input("Press enter to continue...")
