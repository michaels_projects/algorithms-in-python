# import the random library
import random

# make an List of items
def makeList():
    # ask how many items you want
    items = int(input("\nHow many list items would you like?: "))
    # create a new list
    randomList = []
    
    # add items into the list
    for x in range(1,items+1):
        randomList.append(random.randint(0,100))
        
    # return the list
    print("Unsorted: " + str(randomList))
    return(randomList)

def insertionSort(list_a):
    indexing_length = range(1, len(list_a))
    for i in indexing_length:
        value_to_sort = list_a[i]
        
        while list_a[i-1] > value_to_sort and i>0:
            list_a[i], list_a[i-1] = list_a[i-1], list_a[i]
            i = i-1

    return list_a

print("  Sorted: " + str(insertionSort(makeList())) + "\nInsertion Sort Complete")
input("Press enter to continue...")
