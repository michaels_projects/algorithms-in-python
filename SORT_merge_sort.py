# import the random library
import random

# make an List of items
def makeList():
    # ask how many items you want
    items = int(input("\nHow many list items would you like?: "))
    # create a new list
    randomList = []
    
    # add items into the list
    for x in range(1,items+1):
        randomList.append(random.randint(1,100))
        
    # return the list
    print("Unsorted: " + str(randomList))
    return(randomList)

def mergeSort(list_a):

   # print("Splitting ",list_a)

   if len(list_a)>1:
       mid = len(list_a)//2
       lefthalf = list_a[:mid]
       righthalf = list_a[mid:]

       #recursion
       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=0
       j=0
       k=0

       while i < len(lefthalf) and j < len(righthalf):
           if lefthalf[i] < righthalf[j]:
               list_a[k]=lefthalf[i]
               i=i+1
           else:
               list_a[k]=righthalf[j]
               j=j+1
           k=k+1

       while i < len(lefthalf):
           list_a[k]=lefthalf[i]
           i=i+1
           k=k+1

       while j < len(righthalf):
           list_a[k]=righthalf[j]
           j=j+1
           k=k+1
       return(list_a)

   # print("Merging   ",list_a)

print("  Sorted: " + str(mergeSort(makeList())) + "\nInsertion Sort Complete")
input("Press enter to continue...")
